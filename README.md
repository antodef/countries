# AngularJS Web Application
# The application allow allow to visualize the list of countries or download it as a .csv file

### Installing Dependencies

You can install dependencies by running:

```
npm install
```

### Running the Application:

```
npm start
```