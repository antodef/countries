'use strict';

angular.
  module('operations').
  component('operations', {
    templateUrl: 'operations/operations.template.html',
    controller: ['Countries',
      function OperationsController(Countries) {
        var self = this;

        self.visualize = function(){
          window.location.replace('#!/countries');
        }

      }
    ]
  });
