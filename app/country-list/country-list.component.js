'use strict';

angular.
  module('countryList').
  component('countryList', {
    templateUrl: 'country-list/country-list.template.html',
    controller: ['Countries',
      function CountryListController(Countries) {

        var self = this;
        
        Countries.getCountries().then(
            function callback(response) {
                self.countries = response;
            });

        self.back = function(){
          window.location.replace('#!/operations');
        }
        
      }
    ]
  });
