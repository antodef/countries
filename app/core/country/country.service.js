'use strict';

angular.
  module('core.country').
  factory('Country', function() {

      function Country(code, name) {
        this.code = code;
        this.name = name;
      }
      
      return Country;
    }
  );
   