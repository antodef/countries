'use strict';

var url = 'http://pastebin.com/raw/943PQQ0n';
var dataStartingRow = 3;
var splitFormat = "   ";

angular.
  module('core.countries').
  factory('Countries', ['$http','$q','Country',
    function($http,$q,Country) {

      var CountriesService = {};

      CountriesService.getCountries = function getData(){
        return getFile()
          .then(processFile)
          .then(formatCountryList)
      };


      function getFile() {
        return $http.get(url);
      };

      function processFile(response){
        var fileList = response.data.split('\n').slice(dataStartingRow);
        return fileList;
      };

      function formatCountryList(fileList) {
        var countryList = [];
        var code;
        var name;
        var countryRow;
        for (var i = 0; i < fileList.length; i++) {
          countryRow = fileList[i].split(splitFormat);
          code=countryRow[0];
          name=countryRow[1];
          var country = new Country(code, name);
          countryList.push(country);
        }
        return countryList;
      };


      return CountriesService;
    }
  ]);