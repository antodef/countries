'use strict';

angular.module('foodoraApp', [
  'ngAnimate',
  'ngRoute',
  'core',
  'operations',
  'countryList',
  'ngSanitize', 
  'ngCsv'
]);
