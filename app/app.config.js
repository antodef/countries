'use strict';

angular.
  module('foodoraApp').
  config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/countries', {
          template: '<country-list></country-list>'
        }).
        when('/operations', {
          template: '<operations></operations>'
        }).
        otherwise('/operations');
    }
  ]);
